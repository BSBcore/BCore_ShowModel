using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCube : MonoBehaviour
{
    float rotateX;
    float rotateY;
    float rotateZ = -4;
    void Start()
    {
        Camera.main.transform.position = transform.position + transform.rotation * Quaternion.Euler(transform.rotation.y, transform.rotation.x, 0) * new Vector3(0, 2, rotateZ);
        Camera.main.transform.LookAt(transform);
    }
    void Update()
    {
        RotateAroundCube();
    }
    void RotateAroundCube()
    {
        if (Input.GetMouseButton(0) || Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            float x = Input.GetAxis("Mouse X");
            rotateX += x * 5;
            Debug.Log("rotateX" + rotateX);
            float y = Input.GetAxis("Mouse Y");
            rotateY += y * 5;
            Debug.Log("rotateY" + rotateY);
            //防止出现翻转现象
            rotateY = Mathf.Clamp(rotateY, -80, 80);
            rotateZ += Input.GetAxis("Mouse ScrollWheel") * 5;
            rotateZ = Mathf.Clamp(rotateZ, -8, -2);
            Camera.main.transform.position = transform.position + transform.rotation * Quaternion.Euler(rotateY, rotateX, 0) * new Vector3(0, 2, rotateZ);
            Camera.main.transform.LookAt(transform);
        }
    }
}
