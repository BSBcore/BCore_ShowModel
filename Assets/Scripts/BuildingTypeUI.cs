
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UIFrameWork;
using WJExcelDataClass;
using System;

public class BuildingTypeUI : BaseUI
{
    public RawImage modelImg;
    public Button btnShowCloths;
    public Button btnShowFloorCount;
    public Transform modelParent;
    public GameObject clothParent;
    public GameObject[] prefabs;
    public float modelPadding = 0.25f;
    private int mPrefabCount;
    private int minFloor = 4;
    public GameObject mItemStylePrefab;
    public Transform mItemStyleParent;
    public GameObject go2DView;
    public GameObject goModelDesc;
    public Text txtNameTitle;
    public Text txtModelDesc;
    public Image modelInfoImg;
    public GameObject goPriceDesc;
    public Text txtPriceDesc;
    public GameObject goClothView;
    public GameObject goFloorView;
    public Text txtPrice;
    public GameObject goAInfoView;
    public GameObject IMG_seriesA;
    public GameObject[] floorToggles;//楼层数开关.
    public OrbitCamera cam;
    public MyToggleGroup MTG;
    public MyToggleGroup MTGCloth;
    public GameObject OrderView;
    public Text txtOrderDesc;




    void Awake()
    {
        //定义窗口性质 (默认数值)
        currentUIType.type = UIFormType.Normal;
        currentUIType.mode = UIFormShowMode.Normal;
        currentUIType.lucenyType = UIFormLucenyType.Lucency;
       // EventTriggerListener.Get(modelImg.gameObject).onClick += OnModelClick;
        EventTriggerListener.Get(IMG_seriesA.gameObject).onClick += OnSeriesAClick;
        MTG.onValueChanged.AddListener(OnFloorToggleChange);
        MTGCloth.value = 0;
        MTGCloth.onValueChanged.AddListener(OnClothToggleChange);
     

        //注册按钮
        //RigisterBtnOnClick("btnLogin", go =>
        //{
        //    OpenUI(ProConst.SelectUI);
        //});
    }
    // Start is called before the first frame update
    void Start()
    {
        InitData();
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    List<MyItemModel> mItemModelList = new List<MyItemModel>();
    private ModelItem mModelData;//当前旋转的模型数据.
    public void InitData()
    {
        DataManager DM = new DataManager();
        DM.LoadAll();
        Dictionary<int, ModelItem> dic = DM.p_Model.Dict;
        mItemModelList.Clear();
        foreach (var item in dic)
        {
            ModelItem mi = item.Value;
            MyItemModel myItemModel = new MyItemModel(mi, mItemStylePrefab, mItemStyleParent);
            myItemModel.callBack = OnBuildStyleClick;
            mItemModelList.Add(myItemModel);
        }
        mItemStyleParent.GetComponent<RectTransform>().sizeDelta = new Vector2(80*dic.Count,0);
        mModelData = mItemModelList[0].ItemData;
        SetCurBuild();
    }
    private void SetCurBuild()
    {
       
        ClearChild(modelParent);
        ClearChild(clothParent.transform);
        int floorCount = mModelData.floorCount[0];
        mPrefabCount = floorCount / 4;
        RefreshFloorToggles();
        goClothView.SetActive(false);
        StartCoroutine(DelayBuild(GetModelPrefab()));
    
    }
   
    //点击了楼型选择.
    private void OnBuildStyleClick(ModelItem item)
    {
        mModelData = item;
        SetCurBuild();
        OnSelectBuildItem(item);
      
    }
    //选中状态.
    private void OnSelectBuildItem(ModelItem item)
    {
        for (int i = 0; i < mItemModelList.Count; i++)
        {
            MyItemModel mi = mItemModelList[i];
            mi.SetSelect(mi.ItemData == item);
        }
    }
    //点击了模型名称.
    public void OnModelClick()
    {
        goModelDesc.SetActive(true);
        txtNameTitle.text = mModelData.modelNameCH;
        txtModelDesc.text = mModelData.modelDesc;
        modelInfoImg.sprite= Resources.Load("UI/ModelIcon/" + mModelData.icon2D, typeof(Sprite)) as Sprite;
    }
    //点击了A系列.
    public void OnSeriesAClick(GameObject go)
    {
        goAInfoView.SetActive(true);
    }
    //点击了楼层按钮.
    public void OnBtnFloorClick()
    {
        goFloorView.SetActive(true);
        goClothView.SetActive(false);
        
       // RefreshFloorToggles();
    }
    public void ShowOrderView()
    {
        if (mModelData == null) return;
        OrderView.gameObject.SetActive(true);
        txtOrderDesc.text =
              "楼型名：                                                         "+ mModelData.modelNameCH +
            "\n" +
             "楼层数：                                                          "+ totalFloor +
            "\n幕墙编号：                                                     1001" +
            "\n" +
             "总造价：                                                        "+ strSumPrice;

    }

    public void OnBtnOrderClick()
    {
        Application.OpenURL("http://chat.bsbcore.com/channel/%25E8%258A%25AF%25E9%2580%2589%25E5%25AE%25A2%25E6%2588%25B7%25E8%25AE%25A2%25E5%258D%2595");
    }
    private void RefreshFloorToggles()
    {
        int count = mModelData.floorCount.Count;
        for (int i = 0; i < floorToggles.Length; i++)
        {
            GameObject goToggle = floorToggles[i];
            
            for (int j = 0; j < count; j++)
            {
              
                Text txtFloor = floorToggles[j].transform.Find("txtFloor").GetComponent<Text>();
                txtFloor.text = mModelData.floorCount[j] + "F";
            }
        }
       // floorToggles[0].GetComponent<Toggle>().isOn = true;
       if(MTG.value!=0)
        {
            MTG.value = 0;
        }
        MTGCloth.value = 0;

    }
    //点击了幕墙按钮.
    public void OnBtnClothClick()
    {
        goClothView.SetActive(mIsBuilded);//需要创建完成才可以贴幕墙.
        goFloorView.SetActive(false);
    }

    
    //选中了幕墙.
    public void OnClothToggleChange(int value)
    {
      
        ClearChild(clothParent.transform);
        OnCreateCloth();
        StartCoroutine(OnCreateCloth());
    }
    IEnumerator OnCreateCloth()
    {
       
        for (int i = 0; i < totalFloor-1; i++)
        {
            GameObject go = Instantiate(Resources.Load("Prefab/" + mModelData.modelName + "_muqiang") as GameObject);
            go.transform.SetParent(clothParent.transform);
            go.transform.localPosition = new Vector3(0, 0.079937f * (i+1), 0);
            go.transform.localScale = Vector3.one;
            yield return new WaitForSeconds(0.1f);
        }
        GameObject top = Instantiate(Resources.Load("Prefab/" + mModelData.modelName + "_Top") as GameObject);
        top.transform.SetParent(clothParent.transform);
        top.transform.localPosition = new Vector3(0, 0.079937f * totalFloor, 0);
        top.transform.localScale = Vector3.one;
        if(!mModelData.modelName.Contains("AK"))
        {
            GameObject wuding = Instantiate(Resources.Load("Prefab/Base_wuding") as GameObject);
            wuding.transform.SetParent(clothParent.transform);
            wuding.transform.localPosition = new Vector3(0, 0.079937f * totalFloor+0.02f, 0);
            wuding.transform.localScale = Vector3.one;
        }
    }
    private int curFloorIndex = 0;
    public void OnFloorToggleChange(int floorIndex)
    {
        curFloorIndex = floorIndex;
        int floorCount = mModelData.floorCount[floorIndex];
        mPrefabCount = floorCount / 4;
        ClearChild(modelParent);
        ClearChild(clothParent.transform);
        StartCoroutine(DelayBuild(GetModelPrefab()));
        
    }

   
    public void OnResetCam()
    {
        cam.Reset();
        
    }

    //点击了2D平面展示
    public void OnBtn2DClick()
    {
        go2DView.SetActive(true);

    }
   
    private void GetPrice()
    {
        float sum = mModelData.price1 + mModelData.price2 + mModelData.price3 + mModelData.price4 + mModelData.price5 + mModelData.price6 + mModelData.price7 + mModelData.price8;
        float totalSum = sum * mModelData.area * totalFloor;
        Math.Round(Convert.ToDecimal(45.367), 2, MidpointRounding.AwayFromZero);
        if (totalSum > 100000000f)
        {
            strSumPrice = Math.Round(Convert.ToDecimal(totalSum / 100000000f), 2, MidpointRounding.AwayFromZero) + "亿";
        }
        else if (totalSum > 10000000f)
        {
            strSumPrice = Math.Round(Convert.ToDecimal(totalSum / 10000000f), 2, MidpointRounding.AwayFromZero) + "千万";
        }
        else if (totalSum > 1000000f)
        {
            strSumPrice = Math.Round(Convert.ToDecimal(totalSum / 1000000f), 2, MidpointRounding.AwayFromZero) + "百万";
        }
    }
    private string strSumPrice;
    public void OnBtnPriceClick()
    {
        goPriceDesc.SetActive(true);


        string str = "地下室工程                                                                 " + mModelData.price1.ToString() + "\n"
                    + "主体结构工程                                                             " + mModelData.price2.ToString() + "\n"
                    + "围护结构                                                                      " + mModelData.price3.ToString() + "\n"
                    + "装饰装修                                                                      " + mModelData.price4.ToString() + "\n"
                    + "机电安装工程                                                             " + mModelData.price5.ToString() + "\n"
                    + "暖通排风                                                                      " + mModelData.price6.ToString() + "\n"
                    + "室外工程                                                                      " + mModelData.price7.ToString() + "\n"
                    + "不可预计费                                                                  " + mModelData.price8.ToString() + "\n" + "\n"
                    + "总计                                                                              " + strSumPrice;
        txtPriceDesc.text = str;
    }


   
    private GameObject GetModelPrefab()
    {
        for (int i = 0; i < prefabs.Length; i++)
        {
            if (prefabs[i].name == mModelData.modelName)
            {
                return prefabs[i];
            }
        }
        return prefabs[0];
    }
    bool mIsBuilded = false;
    IEnumerator DelayBuild(GameObject go)
    {
        mIsBuilded = false;
        int count = mPrefabCount;//未选中楼层，默认第一个楼层.
        totalFloor = count * 4;
        for (int i = 0; i < count; i++)
        {
            GameObject itemObj = Instantiate(go) as GameObject;
            itemObj.transform.parent = modelParent;
            itemObj.transform.localScale = Vector3.one;
            itemObj.transform.localPosition = new Vector3(0, 0.31975f * i, 0);
            itemObj.SetActive(true);
            GameObject padiao = Instantiate(Resources.Load("Prefab/padiao_ani") as GameObject);
            padiao.transform.parent = itemObj.transform;
            padiao.transform.localScale = Vector3.one;
            padiao.transform.localPosition = new Vector3(0, 0.005f, 0);
            yield return new WaitForSeconds(0.3f);
            Destroy(padiao);
        }
        GetPrice();
        txtPrice.text = strSumPrice;
        mIsBuilded = true;
    }
    private void ClearChild(Transform parent)
    {
       
        StopAllCoroutines();
        for (int i = parent.childCount - 1; i >= 0; i--)
        {
            Transform t = parent.GetChild(i);
            Destroy(t.gameObject);
        }
      
       
    }

  
    private int totalFloor = 0;
    //选择层数
  

    //放大缩小
    public void OnBtnZoomClick(bool isIn)
    {
        if (isIn)
        {
            if (modelParent.localScale.x >= 0.2f) return;
            modelParent.localScale = modelParent.localScale * 1.1f;
        }
        else
        {
            if (modelParent.localScale.x < 0.05f) return;
            modelParent.localScale = modelParent.localScale * 0.9f;
        }
    }
    public void OnBtnMoreClick()
    {
        Application.OpenURL("http://chat.bsbcore.com/");
    }


}
