using UnityEngine;
using UnityEngine.EventSystems;

public class TouchCamera : MonoBehaviour
{
    private float minScale = 0.3f;
    private float maxScale = 3f;

    private Touch oldTouch1;  //上次触摸点1(手指1)  
    private Touch oldTouch2;  //上次触摸点2(手指2)  

    void Update()
    {

        //没有触摸  
        if (Input.touchCount <= 0)
        {
            return;
        }

        ////单点触摸， 水平上下移动
        //if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
        //{
        //    //EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)为true表示点到了UI
        //    if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
        //    {
        //        var deltaposition = Input.GetTouch(0).deltaPosition;
        //        transform.Translate(-deltaposition.x * 0.1f, 0f, -deltaposition.y * 0.1f);
        //    }
        //}
        //单点触摸， 水平上下旋转  
        if (1 == Input.touchCount)
        {

            if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
                Touch touch = Input.GetTouch(0);
                Vector2 deltaPos = touch.deltaPosition;
                transform.Rotate(Vector3.down * deltaPos.x, Space.World);
                transform.Rotate(Vector3.right * deltaPos.y, Space.World);
            }
        }

        //多点触摸, 放大缩小  
        Touch newTouch1 = Input.GetTouch(0);
        Touch newTouch2 = newTouch1;
        Debug.Log(Input.touchCount);
        if (Input.touchCount == 2)
        {
            newTouch2 = Input.GetTouch(1);
            //第2点刚开始接触屏幕, 只记录，不做处理  
            if (newTouch2.phase == TouchPhase.Began)
            {
                oldTouch2 = newTouch2;
                oldTouch1 = newTouch1;
                return;
            }

            //计算老的两点距离和新的两点间距离，变大要放大模型，变小要缩放模型  
            float oldDistance = Vector2.Distance(oldTouch1.position, oldTouch2.position);
            float newDistance = Vector2.Distance(newTouch1.position, newTouch2.position);

            //两个距离之差，为正表示放大手势， 为负表示缩小手势  
            float offset = newDistance - oldDistance;

            //放大因子， 一个像素按 0.01倍来算(100可调整)  
            float scaleFactor = offset / 100f;
            Vector3 localScale = transform.localScale;
            Vector3 scale = new Vector3(localScale.x + scaleFactor,
                                        localScale.y + scaleFactor,
                                        localScale.z + scaleFactor);
            if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)
                && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(1).fingerId))
            {
                //最小缩放到 0.3 倍 ，最大放大到 3 倍
                if (scale.x > minScale && scale.y > minScale && scale.z > minScale && scale.x < maxScale && scale.y < maxScale && scale.z < maxScale)
                {
                    transform.localScale = scale;
                }
                //记住最新的触摸点，下次使用  
                oldTouch1 = newTouch1;
                oldTouch2 = newTouch2;
            }
        }
    }

}

