using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Events;

/// <summary>
/// 单选组或多选组
/// </summary>
[AddComponentMenu("UI/MyToggleGroup", 32)]
public class MyToggleGroup : UIBehaviour
{
    [SerializeField]
    private bool m_AllowSwitchOff = false;
    public bool allowSwitchOff { get { return m_AllowSwitchOff; } set { m_AllowSwitchOff = value; } }

    [SerializeField]
    private List<MyToggle> m_Toggles = new List<MyToggle>();


    [Serializable]
    public class ToggleGroupEvent1 : UnityEvent<int>
    { }

    [Serializable]
    public class ToggleGroupEvent2 : UnityEvent<List<int>>
    { }

    /// <summary>
    /// 单选模式下触发
    /// </summary>
    public ToggleGroupEvent1 onValueChanged = new ToggleGroupEvent1();

    /// <summary>
    /// 多选模式下触发
    /// </summary>
    public ToggleGroupEvent2 onValuesChanged = new ToggleGroupEvent2();

#if UNITY_EDITOR
    protected override void Start()
    {
        base.Start();
        valueList = new List<int>();
        int index = -1;
        for (int i = 0; i < m_Toggles.Count; i++)
        {
            index = valueList.IndexOf(m_Toggles[i].value);
            if (index!=-1)
                throw new Exception(string.Format("同一个Group下不能有相同value的Toggle [{0},{1}]", m_Toggles[index], m_Toggles[i]));
            valueList.Add(m_Toggles[i].value);
        }
    }
#endif

    protected MyToggleGroup()
    { }

    private void ValidateToggleIsInGroup(MyToggle toggle)
    {
        if (toggle == null || !m_Toggles.Contains(toggle))
            throw new ArgumentException(string.Format("Toggle {0} is not part of ToggleGroup {1}", new object[] { toggle, this }));
    }

    public void NotifyToggleOn(MyToggle toggle)
    {
        ValidateToggleIsInGroup(toggle);

        if (!allowSwitchOff)
        {
            if (!toggle.isOn)
                return;
            for (var i = 0; i < m_Toggles.Count; i++)
            {
                if (m_Toggles[i] == toggle)
                    continue;

                m_Toggles[i].isOn = false;
            }
            onValueChanged.Invoke(toggle.value);
        }
        else
            onValuesChanged.Invoke(values);

    }

    public void UnregisterToggle(MyToggle toggle)
    {
        if (m_Toggles.Contains(toggle))
            m_Toggles.Remove(toggle);
    }

    public void RegisterToggle(MyToggle toggle)
    {
        if (!m_Toggles.Contains(toggle))
            m_Toggles.Add(toggle);
    }

    public bool AnyTogglesOn()
    {
        return m_Toggles.Find(x => x.isOn && x.gameObject.activeSelf) != null;
    }
    
    public int value {
        get
        {
            if (allowSwitchOff)
                throw new Exception("多选请使用values");
            MyToggle toggle = m_Toggles.Find((a) => a.isOn && a.gameObject.activeSelf);
            if (toggle)
                return toggle.value;
            else
                return int.MinValue;
        }
        set
        {
            if (allowSwitchOff)
                throw new Exception("多选请使用values");

            for (int i = 0; i < m_Toggles.Count; i++)
            {
                m_Toggles[i].isOn = m_Toggles[i].value == value;
            }
        }
    }

    private List<int> valueList;
    public List<int> values
    {
        get
        {
            if(!allowSwitchOff)
                throw new Exception("单选请使用value");
            if (valueList == null)
                valueList = new List<int>();
            valueList.Clear();
            for (var i = 0; i < m_Toggles.Count; i++)
            {
                if (m_Toggles[i].isOn && m_Toggles[i].gameObject.activeSelf)
                    valueList.Add(m_Toggles[i].value);
            }
            return valueList;
        }
        set
        {
            if (!allowSwitchOff)
                throw new Exception("单选请使用value");
            for (var i = 0; i < m_Toggles.Count; i++)
            {
                m_Toggles[i].isOn = value.Contains(m_Toggles[i].value);
            }
        }
    }

    public void SetAllTogglesOff()
    {
        bool oldAllowSwitchOff = m_AllowSwitchOff;
        m_AllowSwitchOff = true;

        for (var i = 0; i < m_Toggles.Count; i++)
            m_Toggles[i].isOn = false;

        m_AllowSwitchOff = oldAllowSwitchOff;
    }

    public void SetNewGroup(Dictionary<string, int> toggles, int select=-1)
    {
        MyToggle myToggle;
        Text label;
        int count = 0;
        if (toggles!=null && toggles.Count > 0)
        {
            count = toggles.Count;
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
            return;
        }

        var ie = toggles.GetEnumerator();
        for (int i = 0; i < m_Toggles.Count; i++)
        {
            ie.MoveNext();
            myToggle = m_Toggles[i];
            if (i < count)
            {
                myToggle.gameObject.SetActive(true);
                label = myToggle.GetComponentInChildren<Text>();
                if (label)
                    label.text = ie.Current.Key;
                myToggle.value = ie.Current.Value;
            }
            else
                myToggle.gameObject.SetActive(false);
        }

        value = select;
    }

    public void SetAllTogglesDisabled()
    {
        for (var i = 0; i < m_Toggles.Count; i++)
        {
            var myToggle = m_Toggles[i];
            myToggle.interactable = false;
            myToggle.isOn = false;
        }
    }
    public void SetAllTogglesNotInteractable()
    {
        for (var i = 0; i < m_Toggles.Count; i++)
        {
            var myToggle = m_Toggles[i];
            myToggle.interactable = false;
        }
    }

    public void SetAllTogglesEnable(int index)
    {
        for (var i = 0; i < m_Toggles.Count; i++)
        {
            var myToggle = m_Toggles[i];
            myToggle.interactable = true;
        }
        m_Toggles[index].isOn = true;
    }
    public void SetAllTogglesEnable()
    {
        for (var i = 0; i < m_Toggles.Count; i++)
        {
            var myToggle = m_Toggles[i];
            myToggle.interactable = true;
        }
    }
}
