using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Orbit : MonoBehaviour
{

    private float x = 0, y = 0;

    public float rotateSpeed = 5f;

    private Transform mTrans;

    private Vector3 distanceVec;

    public Transform player;
    // Use this for initialization
    void Start()
    {
        mTrans = GetComponent<Transform>();
        distanceVec = mTrans.position - player.position;
    }

    // Update is called once per frame
    void Update()
    {

    }
    void LateUpdate() //对摄像机的操作写在LateUpdate里
    {
        MOVE();
        // move2();
    }

    private void move2()
    {
        x += Input.GetAxis("Mouse X") * rotateSpeed * Time.deltaTime;
        y -= Input.GetAxis("Mouse Y") * rotateSpeed * Time.deltaTime;
        mTrans.RotateAround(player.position, new Vector3(x, y, 0), 20 * Time.deltaTime);
    }

    private void MOVE()
    {
        x += Input.GetAxis("Mouse X") * rotateSpeed * Time.deltaTime;
        y -= Input.GetAxis("Mouse Y") * rotateSpeed * Time.deltaTime;
        y = Mathf.Clamp(y, -60, 80);
        //根据人物位置确定摄像机的位置，这里用四元数进行了旋转 
        Quaternion camPosRotation = Quaternion.Euler(y, x, 0);
        // mTrans.rotation = camPosRotation;
        mTrans.position = Vector3.Lerp(mTrans.position, (camPosRotation * distanceVec + player.position), Time.deltaTime * 5f);
        //camPosRotation* distanceVec返回一个Vector3朝向你的四元数的方向headRotation。
        //得到一个朝向你所要旋转方向的一个向量
        Debug.Log(camPosRotation * distanceVec);
        mTrans.LookAt(player.position);
    }
}
